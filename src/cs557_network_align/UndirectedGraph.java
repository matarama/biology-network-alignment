package cs557_network_align;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class UndirectedGraph extends Graph 
{
	public UndirectedGraph()
	{
		super();
	}

	public void addEdge(String from, String to)
	{
		addVertex(from);
		addVertex(to);
		
		int fromIndex = idToIndex.get(from);
		int toIndex = idToIndex.get(to);
		
		adjList.get(fromIndex).add(toIndex);
		adjList.get(toIndex).add(fromIndex);
	}
	
	public int getNeighborCount(int v)
	{
		return adjList.get(v).size();
	}
	
	public int getOutDegree(int v)
	{
		return getNeighborCount(v);
	}
	
	public int getInDegree(int v)
	{
		return getNeighborCount(v);
	}

	public Graph readPPI(String path) throws FileNotFoundException, IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(path));
		reader.readLine();
		
		String line = reader.readLine();
		while(line != null)
		{
			String[] vId = line.split("\t");
			this.addEdge(vId[0], vId[1]);
			this.addEdge(vId[1], vId[0]);
			
			line = reader.readLine();
		}
		
		reader.close();
		
		return this;
	}
	
	// ---------------------------------------------------------------------------------------

	
	/**
	 * Returs true if v is a neighbor of u, false otherwise.
	 */
	@Override
	public boolean isNeighbor(int v, int u) 
	{
		for(int n : adjList.get(u))
		{
			if(n == v)
				return true;
		}
		
		return false;
	}
	
	
}
