package cs557_network_align;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Netal 
{
	private Graph g1;
	private Graph g2;
	private int noOfIterations;
	private double[][] T; // topology matrix
	private double[][] I; // interaction matrix
	private double[][] D; // definite interaction matrix
	private double[] C1; // Canceled interaction matrix
	private double[] C2; // Canceled interaction matrix
	private double alpha; // 0 <= alpha <= 1, weight for similarity score
	private double beta; // 0 <= beta <= 1, weight for biological similarity score
	private double lambda; // 0 <= lambda <= 1, weight for alignment score
	
	private BlastBitScore blastBitScore; // Blast scores
	private double[][] B; // pairwise biological similarity matrix of proteins
	
	// Similarity Score: S(i, j) = alpha * T(i, j) + (1 - alpha) * B(i, j)
	// where T -> topological similarity, B -> biological similarity
	private double[][] S;
	
	// Alignment Score: A(i, j) = lambda * S(i, j) + (1 - lambda) * I(i, j)
	// where S -> similarity score, I -> interaction score
	private double[][] A;
	
	private int[][] mask;
	private double[][] finalAlignmentScores;
	private Map<Integer, Integer> alignmentMap;
	private Map<String, String> alignmentMapIds;
	
	public Netal(
			Graph g1, Graph g2, int noOfIterations, 
			double alpha, double beta, double lambda,
			String blastBitScorePath)
	{
		this.g1 = g1;
		this.g2 = g2;
		g1.fillStats();
		g2.fillStats();
		
		this.noOfIterations = noOfIterations;
		this.T = createTopologyMatrix(noOfIterations);
		this.I = createInteractionMatrix();
		this.D = new double[g1.getVertexCount()][g2.getVertexCount()];
		this.C1 = new double[g1.getVertexCount()];
		this.C2 = new double[g2.getVertexCount()];
		this.S = new double[g1.getVertexCount()][g2.getVertexCount()];
		this.A = new double[g1.getVertexCount()][g2.getVertexCount()];
		this.alpha = alpha;
		this.beta = beta;
		this.lambda = lambda;
		
		this.B = new double[g1.getVertexCount()][g2.getVertexCount()];
		this.blastBitScore = new BlastBitScore(blastBitScorePath);
		populateBlastBitScores();
		
		
		this.mask = new int[g1.getVertexCount()][g2.getVertexCount()];
		for(int i = 0; i < mask.length; ++i)
			for(int j = 0; j < mask[i].length; ++j)
				mask[i][j] = 1;
		
		this.finalAlignmentScores = new double[g1.getVertexCount()][g2.getVertexCount()];
		this.alignmentMap = new HashMap<Integer, Integer>();
		this.alignmentMapIds = new HashMap<String, String>();
		
		// initialize similarity score
		for(int i = 0; i < S.length; ++i)
			for(int j = 0; j < S[i].length; ++j)
				S[i][j] = T[i][j];
		
		// initialize alignment score
		for(int i = 0; i < A.length; ++i)
			for(int j = 0; j < A[i].length; ++j)
				A[i][j] = lambda * S[i][j] + (1 - lambda) * I[i][j];
	}

	public void populateBlastBitScores()
	{
		for(int i = 0; i < g1.getVertexCount(); ++i)
		{
			String v1Id = g1.getVertexId(i);
			for(int j = 0; j < g2.getVertexCount(); ++j)
			{
				String v2Id = g2.getVertexId(j);
				
				B[i][j] = blastBitScore.getBlastScore(v1Id, v2Id);
			}
		}
	}
	
	public Pair getMaxScoreAlignment()
	{
		Pair p = new Pair(-1, -1);
		double score = -1;
		for(int i = 0; i < A.length; ++i)
		{
			for(int j = 0; j < A[i].length; ++j)
			{
				if(A[i][j] > score)
				{
					score = A[i][j];
					p = new Pair(i, j);
				}
			}
		}
		
		return p;
	}
	
	/**
	 * align vertice
	 * @param u from graph g1
	 * @param v from graph g2
	 */
	public void align(int u, int v)
	{
		alignmentMap.put(u, v);
		finalAlignmentScores[u][v] = A[u][v];
		for(int k = 0; k < mask.length; ++k)
			mask[k][v] = 0;
		for(int k = 0; k < mask[0].length; ++k)
			mask[u][k] = 0;
		
		update_D(u, v);
		update_C(u, v);
		update_I(u, v);
		update_A();
	}
	
	public void update_A()
	{
		// initialize alignment score
		for(int k = 0; k < A.length; ++k)
		{
			for(int l = 0; l < A[k].length; ++l)
			{
				double alignmentScore = 0;
				if(mask[k][l] != 0)
				{
					double biologicalScore = calculateBiologicalScore(k, l);
					double similarityScore = alpha * T[k][l] + (1 - alpha) * biologicalScore;
					alignmentScore = lambda * similarityScore + (1 - lambda) * I[k][l];
				}
				
				A[k][l] = mask[k][l] * (alignmentScore);
			}
		}
	}
	
	/**
	 * @param v node index
	 * @return v's (g1) aligned node in g2 if exists. Returns -1 otherwise.
	 */
	public int getAlignedNode(int v)
	{
		Integer u = alignmentMap.get(v);
		if(u == null)
			return -1;
		
		return u;
	}
	
	public void update_C(int i, int j)
	{
		double iNeighborCount = g1.getNeighborCount(i);
		for(int ni : g1.neighborIndices(i))
		{
			C1[ni] += 1 / iNeighborCount;
		}
		
		double jNeighborCount = g2.getNeighborCount(j);
		for(int nj : g2.neighborIndices(j))
		{
			C2[nj] += 1 / jNeighborCount;
		}
	}
	
	public void update_D(int i, int j)
	{
		for(int n : g1.neighborIndices(i))
		{
			int nAligned = getAlignedNode(n);
			if(nAligned != -1 && g2.isNeighbor(nAligned, j))
			{
				++D[i][j];
			}
		}
	}
	
	public void update_I(int i, int j)
	{
		double I[][] = new double[g1.getVertexCount()][g2.getVertexCount()];
		
		// calculate dependency for i
		double di = 0;
		for(int u : g1.neighborIndices(i))
		{
			di += 1.0 / g1.getNeighborCount(u);
		}
		di -= C1[i];
		
		// calculate dependency for all vertices in g2
		double dj = 0;
		for(int u : g2.neighborIndices(j))
		{
			dj += 1.0 / g2.getNeighborCount(u);
		}
		dj -= C2[j];
		
		I[i][j] = Math.min(di, dj) / Math.max(g1.getMaxOutDegree(), g2.getMaxOutDegree());
	}
	
	public double calculateBiologicalScore(int i, int j)
	{
		// TODO uncomment
		/*
		// pairwise biological similarity of 2 proteins
		double pairwiseScore = B[i][j];	
		
		// biological similarity of neighbors of 2 proteins 
		double neigborSimilarityScore = 0;
		
		// Consider all possible n^2 matchings
		List<Triplet> matchings = new ArrayList<Triplet>();
		for(int ni : g1.adjList.get(i))
			for(int nj : g2.adjList.get(j))
				matchings.add(new Triplet(ni, nj, B[ni][nj]));
		Collections.sort(matchings);
		
		Map<Integer, Integer> lookup = new HashMap<Integer, Integer>();
		for(int k = matchings.size() - 1; k >= 0; --k)
		{
			Triplet match = matchings.get(k);
			if(!(lookup.containsKey(match.proteinIndex1) || 
				lookup.containsKey(match.proteinIndex2)))
			{
				lookup.put(match.proteinIndex1, 0);
				lookup.put(match.proteinIndex2, 0);
				
				neigborSimilarityScore += match.similarityScore;
			}
		}
		neigborSimilarityScore /= Math.max(g1.getNeighborCount(i), g2.getNeighborCount(j));
		
		return beta * pairwiseScore + (1 - beta) * neigborSimilarityScore;
		*/
		
		return B[i][j];
	}
	
	public void start()
	{
		int minVertexCount = Math.min(g1.getVertexCount(), g2.getVertexCount());
		
		for(int it = 0; it < minVertexCount; ++it)
		{
			// printAlignmentScores();
			Pair p = getMaxScoreAlignment();
			int i = p.i;
			int j = p.j;
			
			align(i, j);
		}
		
		// printAlignmentScores();
		// Create alignment map between protein ids
		for(Entry<Integer, Integer> e : alignmentMap.entrySet())
		{
			alignmentMapIds.put(g1.getVertexId(e.getKey()), g2.getVertexId(e.getValue()));
		}
	}
	
	public Bipartite constructBipartite(double[][] T, int i, int j)
	{
		Bipartite bipartite = new Bipartite();
		
		List<Integer> niIndices = g1.neighborIndices(i);
		List<String> ni = g1.neighbors(i);

		List<Integer> njIndices = g2.neighborIndices(j);
		List<String> nj = g2.neighbors(j);
		
		
		for(int k = 0; k < niIndices.size(); ++k)
		{
			for(int l = 0; l < njIndices.size(); ++l)
			{
				Edge e = new Edge(ni.get(k), nj.get(l), T[niIndices.get(k)][njIndices.get(l)]);
				bipartite.addEdge(e);	
			}
		}
	
		return bipartite;
	}
	
	public double[][] createTopologyMatrix(int noOfIterations)
	{
		double[][] T = new double[g1.getVertexCount()][g2.getVertexCount()];
		double[][] Tnext = new double[g1.getVertexCount()][g2.getVertexCount()];
		
		// Initial values
		for(int i = 0; i < T.length; ++i)
			for(int j = 0; j < T[i].length; ++j)
				T[i][j] = 1;
		
		for(int iter = 0; iter < noOfIterations; ++iter)
		{
			// determine T-next(i, j)
			for(int i = 0; i < T.length; ++i)
			{
				int niCount = g1.getNeighborCount(i);
				
				for(int j = 0; j < T[i].length; ++j)
				{
					int njCount = g2.getNeighborCount(j);
					
					// Construct bipartite graph Gb(i, j)
					Bipartite bij = constructBipartite(T, i, j);
					// System.out.println("vertices: " + g1.getVertexId(i) + " " + g2.getVertexId(j));
					// System.out.println(bij);
					
					double score = 0;
					while(!bij.isEmpty())
					{
						Edge maxWeightEdge = bij.getMaxWeightEdge();
						
						score += maxWeightEdge.weight;
						
						bij.removeEdge(maxWeightEdge);
					}
					
					score = score / Math.max(niCount, njCount);
					Tnext[i][j] = score;
				}
			}
			
			double[][] temp = T;
			T = Tnext;
			Tnext = temp;
		}
		
		return T;
	}

	public double[][] createInteractionMatrix()
	{	
		double I[][] = new double[g1.getVertexCount()][g2.getVertexCount()];
		double denom = Math.max(g1.getMaxOutDegree(), g2.getMaxOutDegree());
		
		// calculate dependency for all vertices in g1
		double[] di = new double[g1.getVertexCount()];
		for(int v = 0; v < di.length; ++v)
		{
			di[v] = 0;
			for(int u : g1.neighborIndices(v))
			{
				di[v] += 1.0 / g1.getNeighborCount(u);
			}
		}
		
		// calculate dependency for all vertices in g2
		double[] dj = new double[g2.getVertexCount()];
		for(int v = 0; v < dj.length; ++v)
		{
			dj[v] = 0;
			for(int u : g2.neighborIndices(v))
			{
				dj[v] += 1.0 / g2.getNeighborCount(u);
			}
		}
		
		for(int i = 0; i < g1.getVertexCount(); ++i)
		{
			for(int j = 0; j < g2.getVertexCount(); ++j)
			{
				I[i][j] = Math.min(di[i], dj[j]) / denom;
			}
			
		}
		
		return I;
	}
	
	public void printAlignment()
	{
		for(Entry<Integer, Integer> e : alignmentMap.entrySet())
		{
			System.out.println(g1.getVertexId(e.getKey()) + " - " + g2.getVertexId(e.getValue()));
		}
	}
	
	public void printAlignmentScoreMatrix()
	{
		System.out.println("Alignment Score Matrix");
		for(int i = 0; i < A.length; ++i)
		{
			for(int j = 0; j < A[i].length; ++j)
			{
				System.out.printf("%.02f ", A[i][j]);
			}
			System.out.println();
		}
	}
	
	public double calculateOverallAlignmentScore()
	{
		double score = 0;
		for(int i = 0; i < finalAlignmentScores.length; ++i)
		{
			for(int j = 0; j < finalAlignmentScores[i].length; ++j)
			{
				score += finalAlignmentScores[i][j];
			}
		}
			
		return score;
	}
	
	// -----------------------------------------------------------------------------------
	
	public Map<String, String> getAlignmentMap()
	{
		return this.alignmentMapIds;
	}
}
