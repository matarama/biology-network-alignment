package cs557_network_align;

public class Triplet implements Comparable<Triplet> 
{
	public int proteinIndex1;
	public int proteinIndex2;
	public double similarityScore;
	
	public Triplet(int proteinIndex1, int proteinIndex2, double similarityScore)
	{
		super();
		this.proteinIndex1 = proteinIndex1;
		this.proteinIndex2 = proteinIndex2;
		this.similarityScore = similarityScore;
	}
	
	public Triplet()
	{
		this(-1, -1, 0);
	}

	@Override
	public int compareTo(Triplet o) 
	{
		if(similarityScore > o.similarityScore)
			return 1;
		else if(similarityScore < o.similarityScore)
			return -1;
		else
			return 0;
	}

	public String toString()
	{
		return proteinIndex1 + "-" + proteinIndex2 + ":" + similarityScore;
	}
}
