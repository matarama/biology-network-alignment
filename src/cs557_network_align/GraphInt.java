package cs557_network_align;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface GraphInt {

	public Graph readPPI(String path) throws FileNotFoundException, IOException;
	public void addEdge(String from, String to);
	public int getNeighborCount(int v);
	public int getOutDegree(int v);
	public int getInDegree(int v);
	public boolean isNeighbor(int v, int u);
}
