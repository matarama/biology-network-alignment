package cs557_network_align;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

public class Main 
{
	public static void main(String[] args)
	{
//		Graph g1 = new UndirectedGraph();
//		g1.addVertices("a", "b", "c", "d");
//		g1.addEdge("a", "b");
//		g1.addEdge("b", "c");
//		g1.addEdge("b", "d");

//		Graph g2 = new UndirectedGraph();
//		g2.addVertices("a'", "b'", "c'", "d'", "e'");
//		g2.addEdge("a'", "b'");
//		g2.addEdge("b'", "c'");
//		g2.addEdge("b'", "d'");
//		g2.addEdge("a'", "e'");
		
//		Graph g2 = new UndirectedGraph();
//		g2.addVertices("a'", "b'", "c'", "d'");
//		g2.addEdge("b'", "d'");
//		g2.addEdge("b'", "a'");
//		g2.addEdge("a'", "d'");
//		g2.addEdge("d'", "c'");
//		
//		System.out.println("G1");
//		g1.printAdj();
//		
//		System.out.println();
//		System.out.println("G2");
//		g2.printAdj();
//		System.out.println();
//
//		
//		String blastBitScorePath = "";
//		Netal netal = new Netal(g1, g2, 1, 0.5, 0.5, 0.5, blastBitScorePath);
//		netal.start();
//		netal.printAlignment();
		
//		String ppiPath1 = "data/ppi_networks/filtered/ce.tab";
//		String ppiPath2 = "data/ppi_networks/filtered/mm.tab";
//		String blastBitScorePath = "data/BLAST_Bit_Scores/ce-mm.evals";
		
		String ppiPath1 = "data/ppi_networks/filtered/ce.tab";
		String ppiPath2 = "data/ppi_networks/filtered/dm.tab";
		String blastBitScorePath = "data/BLAST_Bit_Scores/ce-dm.evals";
		
		double alpha = 1; // alpha * TopologicalScore + (1 - alpha) * BiologicalScore
		double beta = 1; // beta * PairwiseBiologicalScore + (1 - beta) * NeighborhoodBiologicalScore
		double lambda = 0.5; // lambda * SimilarityScore + (1 - lambda) * Interaction Score
		
		// for(double lambda = 1; lambda >= 0.0; lambda -= 0.1)
		{
			Graph g1 = new UndirectedGraph();
			Graph g2 = new UndirectedGraph();
			Netal netal = null;
			try {
				g1.readPPI(ppiPath1);
				g2.readPPI(ppiPath2);
				netal = new Netal(g1, g2, 1, alpha, beta, lambda, blastBitScorePath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			long start = System.nanoTime();
			netal.start();
			long end = System.nanoTime();
			
			// netal.printAlignment();
			Map<String, String> alignment = netal.getAlignmentMap();
			// System.out.printf("Overall alignment score: %.3f\n", netal.calculateOverallAlignmentScore());
			// System.out.printf("Normalized alignment score: %.3f\n", netal.calculateOverallAlignmentScore() / alignment.size());
			System.out.println(netal.calculateOverallAlignmentScore());
			System.out.println(netal.calculateOverallAlignmentScore() / alignment.size());
			
			
			// Go enrichment analysis
			String goDatabasePath = "data/__pid.go.goevid.goaspect.txt";
			GODatabase goDatabase = new GODatabase(goDatabasePath);
			double overallGoScore = 0;
			for(Entry<String, String> e : alignment.entrySet())
			{
				overallGoScore += goDatabase.measureGoSimilarity(e.getKey(), e.getValue());
			}
			
			// System.out.printf("Overall Go Score: %.3f\n", overallGoScore);
			// System.out.printf("Normalized Go Score: %.3f\n", (overallGoScore / alignment.size()));
			// System.out.printf("Elapsed Time %.3f seconds.\n", (end - start) / Math.pow(10, 9));
			
			System.out.println(overallGoScore);
			System.out.println((overallGoScore / alignment.size()));
			System.out.println((end - start) / Math.pow(10, 9));
			System.out.println();
		}
	}
}
