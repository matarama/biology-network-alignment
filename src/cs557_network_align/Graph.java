package cs557_network_align;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Graph implements GraphInt 
{
	public List<List<Integer>> adjList;
	public List<String> indexToId;
	public Map<String, Integer> idToIndex;
	
	public int maxOutDegree;
	public int maxInDegree;
	public int minOutDegree;
	public int minInDegree;
	
	public Graph()
	{
		this.adjList = new ArrayList<List<Integer>>();
		this.indexToId = new ArrayList<String>();
		this.idToIndex = new HashMap<String, Integer>();
	}
	
	public String getVertexId(int index)
	{
		return indexToId.get(index);
	}
	
	public int getVertexIndex(String id)
	{
		return idToIndex.get(id);
	}
	
	public void addVertex(String id)
	{
		if(idToIndex.containsKey(id))
			return;
		
		int index = adjList.size();
		idToIndex.put(id, index);
		indexToId.add(id);
		adjList.add(new ArrayList<Integer>());
	}
	
	public void addVertices(String... vertices)
	{
		for(String v : vertices)
		{
			addVertex(v);
		}
	}
	
	public List<String> neighbors(String vertexId)
	{
		return neighbors(getVertexIndex(vertexId));
	}

	public List<String> neighbors(int vertexIndex)
	{
		List<String> n = new ArrayList<String>();
		for(int index : adjList.get(vertexIndex))
			n.add(getVertexId(index));
		
		return n;
	}
	
	public List<Integer> neighborIndices(int v)
	{
		return adjList.get(v);
	}
	
	public List<Integer> neighborIndices(String vertexId)
	{
		return adjList.get(idToIndex.get(vertexId));
	}
	
	public int getVertexCount()
	{
		return indexToId.size();
	}
	
	public int getEdgeCount()
	{
		int edgeCount = 0;
		
		for(int i = 0; i < adjList.size(); ++i)
			edgeCount += adjList.get(i).size();
		
		return edgeCount;
	}
	
	public void fillStats()
	{
		int[] outDegreePerVertex = getOutDegreePerVertex();
		maxOutDegree = outDegreePerVertex[0];
		minOutDegree = outDegreePerVertex[0];
		for(int i = 1; i < outDegreePerVertex.length; ++i)
		{
			if(outDegreePerVertex[i] < minOutDegree)
				minOutDegree = outDegreePerVertex[i];
			else if(outDegreePerVertex[i] > maxOutDegree)
				maxOutDegree = outDegreePerVertex[i];
		}
		
		int[] inDegreePerVertex = getInDegreePerVertex();
		maxInDegree = inDegreePerVertex[0];
		minInDegree = inDegreePerVertex[0];
		for(int i = 1; i < inDegreePerVertex.length; ++i)
		{
			if(inDegreePerVertex[i] < minInDegree)
				minInDegree = inDegreePerVertex[i];
			else if(inDegreePerVertex[i] > maxInDegree)
				maxInDegree = inDegreePerVertex[i]; 
		}
	}
	
	public int[] getOutDegreePerVertex()
	{
		int[] out = new int[adjList.size()];
		for(int i = 0; i < adjList.size(); ++i)
		{
			out[i] = adjList.get(i).size();
		}
		
		return out;
	}
	
	public int[] getInDegreePerVertex()
	{
		int[] in = new int[adjList.size()];
		for(int i = 0; i < adjList.size(); ++i)
		{
			List<Integer> tos = adjList.get(i);
			for(int j = 0; j < tos.size(); ++j)
			{
				++in[tos.get(j)];
			}
		}
		
		return in;
	}
	
	public int getMaxOutDegree()
	{
		return maxOutDegree;
	}
	
	public int getMaxInDegree()
	{
		return maxInDegree;
	}
	
	public int getMinOutDegree()
	{
		return minOutDegree;
	}
	
	public int getMinInDegree()
	{
		return minInDegree;
	}
	
	public void printStats()
	{
		System.out.println("# of vertices: " + getVertexCount());
		System.out.println("# of edges: " + getEdgeCount());
		
		int[] outDegree = getOutDegreePerVertex();
		int[] inDegree = getInDegreePerVertex();
		
		System.out.print("In/Out:");
		for(int i = 0; i < inDegree.length; ++i)
			System.out.println(inDegree[i] + "/" + outDegree[i]);
	}
	
	public void printAdj()
	{
		for(int i = 0; i < adjList.size(); ++i)
		{
			String fromId = indexToId.get(i);
			System.out.print(fromId + " ->");
			
			List<Integer> tos = adjList.get(i);
			for(int j = 0; j < tos.size(); ++j)
			{
				String toId = indexToId.get(tos.get(j));
				System.out.print(" " + toId);
			}
			System.out.println();
		}
	}
	
	public Hypergraph toHypergraph()
	{
		Hypergraph h = new Hypergraph();
		
		// add vetices (nodes)
		for(int i = 0; i < indexToId.size(); ++i)
			h.addNode(indexToId.get(i));
		
		// add edges (hypernodes)
		for(int i = 0; i < adjList.size(); ++i)
		{
			String fromId = indexToId.get(i);
			List<Integer> toList = adjList.get(i);
			for(int j = 0; j < toList.size(); ++j)
			{
				String toId = indexToId.get(toList.get(j));
				
				h.addEdgeFromNodeToNode(fromId, toId);
			}
		}
		
		return h;
	}
}
