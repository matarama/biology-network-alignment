package cs557_network_align;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlastBitScore 
{
	private Map<String, Integer> idToIndex1;
	private Map<String, Integer> idToIndex2;
	private List<String> indexToId1;
	private List<String> indexToId2;
	private double[][] B;
	
	public BlastBitScore(String filePath)
	{
		this.idToIndex1 = new HashMap<String, Integer>();
		this.idToIndex2 = new HashMap<String, Integer>();
		this.indexToId1 = new ArrayList<String>();
		this.indexToId2 = new ArrayList<String>();
		
		try 
		{
			readPairs(filePath);
			B = new double[indexToId1.size()][indexToId2.size()];
			readIntoMatrix(filePath);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		// normalize blast score matrix
		double max = getMaxScore();
		normalize(max);
	}
	
	public double getBlastScore(String p1, String p2)
	{
		Integer i1 = idToIndex1.get(p1);
		Integer i2 = idToIndex1.get(p2);
		
		if(i1 == null || i2 == null)
			return 0;
		
		return B[i1][i2];
	}
	
	public void addPair(String p1, String p2)
	{
		if(!idToIndex1.containsKey(p1))
		{
			int index = indexToId1.size();
			indexToId1.add(p1);
			idToIndex1.put(p1, index);
		}
		
		if(!idToIndex2.containsKey(p2))
		{
			int index = indexToId2.size();
			indexToId2.add(p2);
			idToIndex2.put(p2, index);
		}
	}
	
	public void readIntoMatrix(String filePath) throws FileNotFoundException, IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		
		String line = reader.readLine();
		while(line != null)
		{
			String[] fields = line.split("\t");
			if(!(fields.length < 3))
			{
				double score = Double.parseDouble(fields[2]);
				
				int i1 = idToIndex1.get(fields[0]);
				int i2 = idToIndex2.get(fields[1]);
				B[i1][i2] = score;
			}			
			line = reader.readLine();
		}
		
		reader.close();
	}
	
	public void readPairs(String filePath) throws FileNotFoundException, IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		
		String line = reader.readLine();
		while(line != null)
		{
			String[] fields = line.split("\t");
			if(!(fields.length < 3))
			{
				addPair(fields[0], fields[1]);
			}
			
						
			line = reader.readLine();
		}
		
		reader.close();
	}
	
	public double getMaxScore()
	{
		double max = 0;
		for(int i = 0; i < B.length; ++i)
		{
			for(int j = 0; j < B[i].length; ++j)
			{
				if(max < B[i][j])
					max = B[i][j];
			}
		}
		
		return max;
	}
	
	public void normalize(double denom)
	{
		for(int i = 0; i < B.length; ++i)
		{
			for(int j = 0; j < B[i].length; ++j)
			{
				B[i][j] = B[i][j] / denom;
			}
		}
	}
}
