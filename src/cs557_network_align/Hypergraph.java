package cs557_network_align;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hypergraph 
{
	public List<List<Integer>> adjListNodes;
	public List<List<Integer>> adjListHypernodes;
	
	public List<String> indexToNodeId;
	public Map<String, Integer> nodeIdToIndex;
	public List<String> indexToHypernodeId;
	public Map<String, Integer> hypernodeIdToIndex;
	
	public Hypergraph() 
	{
		super();
		this.adjListNodes = new ArrayList<List<Integer>>();
		this.adjListHypernodes = new ArrayList<List<Integer>>();
		this.indexToNodeId = new ArrayList<String>();
		this.nodeIdToIndex = new HashMap<String, Integer>();
		this.indexToHypernodeId = new ArrayList<String>();
		this.hypernodeIdToIndex = new HashMap<String, Integer>();
	}
	
	public void addNode(String nodeId)
	{
		if(nodeIdToIndex.containsKey(nodeId))
			return;
		
		int nodeIndex = adjListNodes.size();
		adjListNodes.add(new ArrayList<Integer>());
		nodeIdToIndex.put(nodeId, nodeIndex);
		indexToNodeId.add(nodeId);	
	}
	
	public void addHypernode(String hypernodeId)
	{
		if(hypernodeIdToIndex.containsKey(hypernodeId))
			return;
		
		int hypernodeIndex = adjListHypernodes.size();
		adjListHypernodes.add(new ArrayList<Integer>());
		hypernodeIdToIndex.put(hypernodeId, hypernodeIndex);
		indexToHypernodeId.add(hypernodeId);
	}
	
	public void addEdgeFromNodeToHypernode(String nodeId, String hypernodeId)
	{
		addNode(nodeId);
		addHypernode(hypernodeId);
		
		int nodeIndex = nodeIdToIndex.get(nodeId);
		int hypernodeIndex = hypernodeIdToIndex.get(hypernodeId);
		
		adjListNodes.get(nodeIndex).add(hypernodeIndex);
	}
	
	public void addEdgeFromHypernodeToNode(String hypernodeId, String nodeId)
	{
		addHypernode(hypernodeId);
		addNode(nodeId);
		
		int nodeIndex = nodeIdToIndex.get(nodeId);
		int hypernodeIndex = hypernodeIdToIndex.get(hypernodeId);
		
		adjListHypernodes.get(hypernodeIndex).add(nodeIndex);
	}
	
	public int getHyperNodeCount()
	{
		return adjListHypernodes.size();
	}
	
	public int getNodeCount()
	{
		return adjListNodes.size();
	}
	
	public void addEdgeFromNodeToNode(String fromId, String toId)
	{
		String hypernodeId = fromId + "->" + toId;
		
		addEdgeFromNodeToHypernode(fromId, hypernodeId);
		addEdgeFromHypernodeToNode(hypernodeId, toId);
	}
	
	public int[] getInDegreePerNode()
	{
		int[] inDegreePerNode = new int[adjListNodes.size()];
		for(int i = 0; i < adjListHypernodes.size(); ++i)
		{
			for(int to : adjListHypernodes.get(i))
			{
				++inDegreePerNode[to];
			}
		}
		
		return inDegreePerNode;
	}
	
	public int[] getOutDegreePerNode()
	{
		int[] outDegreePerNode = new int[adjListNodes.size()];
		for(int i = 0; i < outDegreePerNode.length; ++i)
		{
			outDegreePerNode[i] = adjListNodes.get(i).size();
		}
		
		return outDegreePerNode;
	}
	
	public int[] getInDegreePerHypernode()
	{
		int[] inDegreePerHypernode = new int[adjListHypernodes.size()];
		
		for(int i = 0; i < adjListNodes.size(); ++i)
		{
			for(int to : adjListNodes.get(i))
			{
				++inDegreePerHypernode[to];
			}
		}
		
		return inDegreePerHypernode;
	}
	
	public int[] getOutDegreePerHypernode()
	{
		int[] outDegreePerHypernode = new int[adjListHypernodes.size()];
		
		for(int i = 0; i < adjListHypernodes.size(); ++i)
		{
			outDegreePerHypernode[i] = adjListHypernodes.get(i).size();
		}
		
		return outDegreePerHypernode;
	}
	
	public Map<String, Integer> getNodeStatistics()
	{
		int[] inDegreePerNode = getInDegreePerNode();
		int[] outDegreePerNode = getOutDegreePerNode();
		
		Map<String, Integer> nodeStatistics = new HashMap<String, Integer>();
		for(int i = 0; i < inDegreePerNode.length; ++i)
		{			
			int inDegree = inDegreePerNode[i];
			int outDegree = outDegreePerNode[i];
			String key = inDegree + "->" + outDegree;
			
			if(nodeStatistics.containsKey(key))
			{
				nodeStatistics.put(key, nodeStatistics.get(key) + 1);
			}
			else
			{
				nodeStatistics.put(key, 1);
			}
		}
		
		return nodeStatistics;
	}
	
	public Map<String, Integer> getHypernodeStatistics()
	{
		int[] inDegreePerHypernode = getInDegreePerHypernode();
		int[] outDegreePerHypernode = getOutDegreePerHypernode();
		
		Map<String, Integer> hypernodeStatistics = new HashMap<String, Integer>();
		for(int i = 0; i < inDegreePerHypernode.length; ++i)
		{
			int inDegree = inDegreePerHypernode[i];
			int outDegree = outDegreePerHypernode[i];
			String key = inDegree + "->" + outDegree;
			
			if(hypernodeStatistics.containsKey(key))
			{
				hypernodeStatistics.put(key, hypernodeStatistics.get(key) + 1);
			}
			else
			{
				hypernodeStatistics.put(key, 1);
			}
		}
		
		return hypernodeStatistics;
	}
	
	public void mergeHypernodeSameTo()
	{
		List<List<Integer>> hypernodesToBeMerged = new ArrayList<List<Integer>>();
		for(int i = 0; i < getNodeCount(); ++i)
		{
			List<Integer> toBeMerged = new ArrayList<Integer>();
			
			for(int j = 0; j < getHyperNodeCount(); ++j)
			{
				for(int to : adjListHypernodes.get(j))
				{
					if(to == i)
						toBeMerged.add(i);
				}
			}
			
			hypernodesToBeMerged.add(toBeMerged);
		}
	}
	
	public void mergeHypernodeSameFrom()
	{
		List<List<Integer>> hypernodesToBeMerged = new ArrayList<List<Integer>>();
	}
	
	public void mergeHypernodeType3()
	{
		
	}
	
	public void mergeHypernodeType4()
	{
		
	}
	
	public void printStats()
	{
		Map<String, Integer> nodeStats = getNodeStatistics();
		System.out.println("# of nodes: " + adjListNodes.size());
		System.out.println("There are " + nodeStats.size() + " node types.");
		System.out.println(nodeStats);
		
		Map<String, Integer> hypernodeStats = getHypernodeStatistics();
		System.out.println("# of hypernodes: " + hypernodeIdToIndex.size());
		System.out.println("There are " + hypernodeStats.size() + " hypernode types.");
		System.out.println(hypernodeStats);
	}
}
