package cs557_network_align;

public class Pair 
{
	int i;
	int j;
	
	public Pair(int i, int j)
	{
		super();
		this.i = i;
		this.j = j;
	}
	
	public Pair()
	{
		this(-1, -1);
	}
	
	public String toString()
	{
		return i + ", " + j;
	}
}
