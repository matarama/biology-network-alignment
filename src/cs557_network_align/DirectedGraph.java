package cs557_network_align;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class DirectedGraph extends Graph 
{
	public DirectedGraph()
	{
		super();
	}
	
	@Override
	public void addEdge(String from, String to)
	{
		addVertex(from);
		addVertex(to);
		
		int fromIndex = idToIndex.get(from);
		int toIndex = idToIndex.get(to);
		
		adjList.get(fromIndex).add(toIndex);
	}
	
	@Override
	public int getNeighborCount(int v) 
	{
		return 0;
	}

	@Override
	public int getOutDegree(int v) 
	{
		return 0;
	}

	@Override
	public int getInDegree(int v) 
	{
		return 0;
	}
	
	// ---------------------------------------------------------------------------------------
	
	public Graph readPPI(String path) throws FileNotFoundException, IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(path));
		reader.readLine();
		
		String line = reader.readLine();
		while(line != null)
		{
			String[] vId = line.split("\t");
			this.addEdge(vId[0], vId[1]);
			
			line = reader.readLine();
		}
		
		reader.close();
		
		return this;
	}

	@Override
	public boolean isNeighbor(int v, int u) {
		// TODO Auto-generated method stub
		return false;
	}


}
