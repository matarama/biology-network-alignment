package cs557_network_align;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GODatabase 
{
	private Map<String, List<Integer>> proteinToGoTerms;
	
	public GODatabase(String path)
	{
		this.proteinToGoTerms = new HashMap<String, List<Integer>>();
		try 
		{
			readGoTerms(path);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public double measureGoSimilarity(String p1, String p2)
	{
		List<Integer> goTerms1 = proteinToGoTerms.get(p1);
		List<Integer> goTerms2 = proteinToGoTerms.get(p2);
		
		if(goTerms1 == null || goTerms2 == null)
			return 0;
		
		Map<Integer, Integer> unionGoTerms = new HashMap<Integer, Integer>();
		for(int i = 0; i < goTerms1.size(); ++i)
			unionGoTerms.put(goTerms1.get(i), 0);
		for(int i = 0; i < goTerms2.size(); ++i)
			unionGoTerms.put(goTerms2.get(i), 0);
		
		List<Integer> commonGoTerms = new ArrayList<Integer>();
		for(int i = 0; i < goTerms1.size(); ++i)
		{
			int term1 = goTerms1.get(i);
			
			for(int j = 0; j < goTerms2.size(); ++j)
			{
				int term2 = goTerms2.get(j);
				
				if(term1 == term2)
				{
					commonGoTerms.add(term1);
				}
			}
		}
		
		return ((double) commonGoTerms.size()) / unionGoTerms.size();
	}
	
	// ----------------------------------------------------------------------------------------
	
	private void readGoTerms(String path) throws FileNotFoundException, IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(path));
		
		String line = reader.readLine();
		while(line != null)
		{			
			if(!line.equals(""))
			{	
				String proteinId = line.substring(0, line.indexOf("\t"));
				List<Integer> goTerms = new ArrayList<Integer>(); 
				
				line = line.substring(line.indexOf("\t") + 1);
				line = line.substring(0, line.indexOf("\t"));
			
				while(line.indexOf("GO:") != -1)
				{
					int goStart = line.indexOf("GO:") + 3;
					int goEnd = line.indexOf("|", goStart);
					if(goEnd < 0)
						goEnd = line.length();
					
					String goId = line.substring(goStart, goEnd);
					
					goTerms.add(Integer.parseInt(goId));					
					line = line.substring(goEnd);
				}
				
				proteinToGoTerms.put(proteinId, goTerms);
			}
			line = reader.readLine();
		}
		
		reader.close();
	}
	
}
