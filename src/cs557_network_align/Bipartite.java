package cs557_network_align;

import java.util.ArrayList;
import java.util.List;

public class Bipartite 
{
	private List<Edge> edges;
	
	public Bipartite()
	{
		this(new ArrayList<Edge>());
	}
	
	public Bipartite(List<Edge> edges)
	{
		super();
		this.edges = edges;
	}
	
	public void addEdge(Edge e)
	{
		edges.add(e);
	}
	
	public boolean isEmpty()
	{
		return edges.isEmpty();
	}
	
	public Edge getMaxWeightEdge()
	{
		int maxWeightIndex = 0;
		for(int i = 1; i < edges.size(); ++i)
		{
			if(edges.get(i).weight > edges.get(maxWeightIndex).weight)
				maxWeightIndex = i;
		}
		
		return edges.get(maxWeightIndex);
	}
	
	public void removeEdge(Edge re)
	{
		int len = edges.size();
		for(int i = len - 1; i >= 0; --i)
		{
			Edge e = edges.get(i);
			if(e.src.equals(re.src) || e.dest.equals(re.dest))
			{
				edges.remove(i);
			}
		}
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		
		for(Edge e : edges)
			sb.append(e.toString() + "\n");
		
		return sb.toString();
	}
}
