package cs557_network_align;

public class Edge implements Comparable<Edge> 
{
	String src;
	String dest;
	double weight;
	
	public Edge()
	{
		this("", "", -1);
	}
	
	public Edge(String src, String dest, double weight)
	{
		super();
		this.src = src;
		this.dest = dest;
		this.weight = weight;
	}
	
	@Override
	public int compareTo(Edge o) 
	{
		if(weight > o.weight)
			return 1;
		else if (weight < o.weight)
			return -1;
		return 0;
	}
	
	public String toString()
	{
		return src + " " + dest + " " + weight;
	}
}
